"""Test fixtures"""

from dotenv import load_dotenv

# load environment variables from .env if present
load_dotenv()
