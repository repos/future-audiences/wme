from wme import auth
from wme import on_demand
import pytest
import httpx
import os


@pytest.mark.asyncio
async def test_simple_fetch():
    async with httpx.AsyncClient() as client:
        username = os.getenv("WME_USERNAME")
        password = os.getenv("WME_PASSWORD")
        response = await auth.login(username, password, client)

        try:
            on_demand_client = on_demand.OnDemand(response.access_token, client)
            articles = await on_demand_client.lookup("Albert Einstein")
            assert len(articles) > 1
        finally:
            await auth.revoke_token(response.refresh_token)


@pytest.mark.asyncio
async def test_weird_fetch():
    async with httpx.AsyncClient() as client:
        username = os.getenv("WME_USERNAME")
        password = os.getenv("WME_PASSWORD")
        response = await auth.login(username, password, client)

        try:
            on_demand_client = on_demand.OnDemand(response.access_token, client)
            weird_name = "Somewhere_Over_the_Rainbow/What_a_Wonderful_World"
            articles = await on_demand_client.lookup_structured(
                weird_name, filters=[on_demand.Filter.for_site("enwiki")]
            )
            assert len(articles) == 1
            assert len(articles[0].article_sections) > 1
        finally:
            await auth.revoke_token(response.refresh_token)


@pytest.mark.asyncio
async def test_plus_fetch():
    async with httpx.AsyncClient() as client:
        username = os.getenv("WME_USERNAME")
        password = os.getenv("WME_PASSWORD")
        response = await auth.login(username, password, client)

        try:
            on_demand_client = on_demand.OnDemand(response.access_token, client)
            weird_name = "C++"
            articles = await on_demand_client.lookup(weird_name)
            assert len(articles) > 1
        finally:
            await auth.revoke_token(response.refresh_token)


@pytest.mark.asyncio
async def test_filter_fetch():
    async with httpx.AsyncClient() as client:
        username = os.getenv("WME_USERNAME")
        password = os.getenv("WME_PASSWORD")
        response = await auth.login(username, password, client)
        try:
            enwiki_filter = on_demand.Filter.for_site("enwiki")
            on_demand_client = on_demand.OnDemand(response.access_token, client)
            articles = await on_demand_client.lookup(
                "Albert Einstein", filters=[enwiki_filter]
            )
            assert len(articles) == 1
        finally:
            await auth.revoke_token(response.refresh_token)


@pytest.mark.asyncio
async def test_structured():
    async with httpx.AsyncClient() as client:
        username = os.getenv("WME_USERNAME")
        password = os.getenv("WME_PASSWORD")
        response = await auth.login(username, password, client)
        try:
            enwiki_filter = on_demand.Filter.for_site("enwiki")
            on_demand_client = on_demand.OnDemand(response.access_token, client)
            articles = await on_demand_client.lookup_structured(
                "Albert Einstein", filters=[enwiki_filter]
            )
            assert len(articles) == 1
            assert articles[0].infobox is not None

            assert articles[0].article_sections is not None
            assert articles[0].article_sections[0].name == "Abstract"
            assert (
                type(articles[0].article_sections[0].has_parts[0])
                is on_demand.ArticleSections
            )

            sections = list(articles[0].iter_sections())
            assert len(sections) > 10
            assert sections[0][0] == "Abstract"
            assert len(sections[0][1].split("\n")) > 1
        finally:
            await auth.revoke_token(response.refresh_token)


@pytest.mark.asyncio
async def test_section_iter():
    async with httpx.AsyncClient() as client:
        username = os.getenv("WME_USERNAME")
        password = os.getenv("WME_PASSWORD")
        response = await auth.login(username, password, client)
        try:
            enwiki_filter = on_demand.Filter.for_site("enwiki")
            on_demand_client = on_demand.OnDemand(response.access_token, client)
            articles = await on_demand_client.lookup_structured(
                "Donald Trump", filters=[enwiki_filter]
            )
            assert len(articles) == 1

            sec_count = 0
            for sec in articles[0].iter_sections():
                sec_count += 1
            assert sec_count > 1
        finally:
            await auth.revoke_token(response.refresh_token)


def test_section_rename():
    api_response = dict(
        sections=[
            dict(
                name="Abstract",
                type=on_demand.SectionType.section,
                has_parts=[
                    dict(
                        type="paragraph",
                        value="This is a test abstract. It is not real.",
                    )
                ],
            )
        ]
    )

    resp = on_demand.EnterpriseAPIResponse.model_validate(api_response)
    assert len(resp.article_sections) == 1

    api_response = dict(
        article_sections=[
            dict(
                name="Abstract",
                type=on_demand.SectionType.section,
                has_parts=[
                    dict(
                        type="paragraph",
                        value="This is a test abstract. It is not real.",
                    )
                ],
            )
        ]
    )

    resp2 = on_demand.EnterpriseAPIResponse.model_validate(api_response)
    assert len(resp2.article_sections) == 1
    assert resp == resp2


@pytest.mark.asyncio
async def test_enwiki_redirect():
    async with httpx.AsyncClient() as client:
        username = os.getenv("WME_USERNAME")
        password = os.getenv("WME_PASSWORD")
        response = await auth.login(username, password, client)
        try:
            on_demand_client = on_demand.OnDemand(response.access_token, client)
            assert "Google Shopping" == await on_demand_client._check_enwiki_redirect(
                "Froogle"
            )

            assert "Food" == await on_demand_client._check_enwiki_redirect("food")

            res = await on_demand_client.lookup_enwiki("Froogle")
            assert res is not None
            assert res.identifier == 401401
            assert res.description is None

            res = await on_demand_client.lookup_enwiki_structured("Froogle")
            assert res is not None
            assert res.identifier == 401401
            assert res.description is not None
        finally:
            await auth.revoke_token(response.refresh_token)
