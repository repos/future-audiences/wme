from wme import auth
import pytest
import os


@pytest.mark.asyncio
async def test_login():
    username = os.getenv("WME_USERNAME")
    password = os.getenv("WME_PASSWORD")
    try:
        response = await auth.login(username, password)
    finally:
        await auth.revoke_token(response.refresh_token)
    assert response.id_token
    assert response.access_token
    assert response.refresh_token
    assert response.expires_in > 0


@pytest.mark.asyncio
async def test_refresh():
    username = os.getenv("WME_USERNAME")
    password = os.getenv("WME_PASSWORD")
    response = await auth.login(username, password)
    try:
        refresh_response = await auth.refresh_token(username, response.refresh_token)
    finally:
        await auth.revoke_token(response.refresh_token)
    assert refresh_response.access_token
    assert refresh_response.expires_in > 0
